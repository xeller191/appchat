import React, {Component} from 'react';
import {Text, View, StyleSheet, StatusBar, Dimensions} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {TouchableOpacity} from 'react-native-gesture-handler';
const {width, height} = Dimensions.get('window');
const one_width = width/411.42857142857144
const one_height = height/797.7142857142857

export default class SplashComponent extends Component {
  async componentDidMount(){
    await alert("H/R"+ "=" + one_height)

}
  render() {

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <View style={styles.header}>
          <Animatable.Image
            animation="bounceIn"
            duration={3000}
            style={styles.logo}
            source={require('../asset/logo.png')}
            resizeMode="stretch"
          />
        </View>
        <Animatable.View style={styles.footer} animation="fadeInUpBig">
          <Text style={styles.title} allowFontScaling={false}>Stay connect with everyone</Text>
          <Text style={styles.text} allowFontScaling={false}>Sign in with account</Text>
          <View style={styles.button}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('SignInComponent');
              }}>
              <LinearGradient
                colors={['#5db8fe', '#39cff2']}
                style={styles.signIn}>
                <Text style={styles.textSignIn} allowFontScaling={false}>Get Started</Text>
                <MaterialIcons name="navigate-next" size={one_height*20} color="white" />
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </View>
    );
  }
}
const height_logo = height * 0.7 * 0.4;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#05375a',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: one_height*30,
    borderTopRightRadius: one_height*30,
    paddingVertical: one_height*50,
    paddingHorizontal: one_height*30,
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },
  title: {
    color: '#05375a',
    fontWeight: 'bold',
    fontSize: one_height*30,
  },
  text: {
    color: 'grey',
    marginTop: one_height*5,
    fontSize:one_height*18
  },
  button: {
    alignItems: 'flex-end',
    marginTop: one_height*30,
  },
  signIn: {
    width: one_width*150,
    height: one_height*40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: one_height*50,
    flexDirection: 'row',
  },
  textSignIn: {
    color: 'white',
    fontWeight: 'bold',
    fontSize:one_height*18
  },
});
