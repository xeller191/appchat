import React, {Component} from 'react';
import {Text, View, StyleSheet, Dimensions} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';

const {width, height} = Dimensions.get('window');
const one_width = width / 411.42857142857144;
const one_height = height / 797.7142857142857;

export default class SignUpComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      inputSelection: {
        start: 0,
        end: 0,
      },
      secureTextEntry: true,
      secureTextEntry_confirm: true,
      email: '',
      password: '',
      password_confirm: '',
      isEmailCorrect: false,
    };
  }
  secureTextEntryFn = () => {
    this.setState({secureTextEntry: !this.state.secureTextEntry});
  };
  secureTextEntryFn_confirm = () => {
    this.setState({
      secureTextEntry_confirm: !this.state.secureTextEntry_confirm,
    });
  };
  textOnchange = (value) => {
    if (value === 'username') {
      this.setState({isEmailCorrect: true});
    } else {
      this.setState({isEmailCorrect: false});
    }
  };

  render() {
    const {inputSelection, input} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Animatable.Text
            style={styles.header_text}
            animation="fadeInDownBig"
            allowFontScaling={false}>
            Welcome to App Chat!
          </Animatable.Text>
        </View>
        <Animatable.View style={styles.footer} animation="fadeInUpBig">
          <Text style={styles.footer_text}>E-MAIL</Text>
          <View style={styles.action}>
            <FontAwesome
              name="user"
              size={one_height * 30}
              color="#05375a"
              style={{marginTop: one_height * 5}}
            />
            <TextInput
              blurOnSubmit={true}
              allowFontScaling={false}
              autoCapitalize="none"
              value={this.state.email}
              placeholder="Your email..."
              style={styles.text_footer_textInput}
              selection={inputSelection}
              onSelectionChange={({nativeEvent: selection}) =>
                this.setState({inputSelection: selection})
              }
              onChangeText={(value) => {
                this.setState({email: value}), this.textOnchange(value);
              }}
              onEndEditing={() =>
                this.setState({inputSelection: {start: 0, end: 0}})
              }
            />
            {this.state.isEmailCorrect ? (
              <AntDesign
                name="checkcircleo"
                color="green"
                size={one_height * 25}
              />
            ) : null}
          </View>

          <Text
            style={[styles.footer_text, {marginTop: one_height * 35}]}
            allowFontScaling={false}>
            Password
          </Text>
          <View style={styles.action}>
            <MaterialIcons name="lock" size={one_height * 30} color="#05375a" />
            <TextInput
              blurOnSubmit={true}
              value={this.state.password}
              allowFontScaling={false}
              autoCapitalize="none"
              secureTextEntry={this.state.secureTextEntry}
              placeholder="Your password..."
              style={styles.text_footer_textInput}
              onChangeText={(value) => {
                this.setState({password: value});
              }}
            />
            {this.state.secureTextEntry ? (
              <TouchableOpacity
                onPress={() => {
                  this.setState({secureTextEntry: !this.state.secureTextEntry});
                }}>
                <Feather name="eye-off" color="grey" size={one_height * 25} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  this.setState({secureTextEntry: !this.state.secureTextEntry});
                }}>
                <Feather name="eye" color="green" size={one_height * 25} />
              </TouchableOpacity>
            )}
          </View>

          {/* confirm password A */}
          <Text
            style={[styles.footer_text, {marginTop: one_height * 35}]}
            allowFontScaling={false}>
            Confirm Password
          </Text>
          <View style={styles.action}>
            <MaterialIcons name="lock" size={one_height * 30} color="#05375a" />
            <TextInput
              blurOnSubmit={true}
              value={this.state.password_confirm}
              allowFontScaling={false}
              autoCapitalize="none"
              secureTextEntry={this.state.secureTextEntry_confirm}
              placeholder="Your password..."
              style={styles.text_footer_textInput}
              onChangeText={(value) => {
                this.setState({password_confirm: value});
              }}
            />
            {this.state.secureTextEntry_confirm ? (
              <TouchableOpacity
                onPress={() => {
                  this.setState({secureTextEntry_confirm: !this.state.secureTextEntry_confirm});
                }}>
                <Feather name="eye-off" color="grey" size={one_height * 25} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  this.setState({secureTextEntry_confirm: !this.state.secureTextEntry_confirm});
                }}>
                <Feather name="eye" color="green" size={one_height * 25} />
              </TouchableOpacity>
            )}
          </View>

          {/* confirm password B */}

          <View style={styles.bottom}>
            <TouchableOpacity
              style={{
                ...styles.signIn,
                borderColor: '#4dc2f8',
                borderWidth: one_height * 1,
                marginTop: one_height * 15,
              }}
              onPress={() => {
                this.props.navigation.goBack(),console.log(this.state.email,this.state.password,this.state.password_confirm);
              }}>
              <LinearGradient
                colors={['#5db8fe', '#39cff2']}
                style={styles.signIn}>
                <Text style={styles.textSignIn} allowFontScaling={false}>
                  Sign Up
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#05375a',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: one_width * 20,
    paddingBottom: one_height >=0.6 ? one_height * 50 :one_height * 25
  },
  footer: {
    flex: 3,
    backgroundColor: 'white',
    borderTopLeftRadius: one_width * 30,
    borderTopRightRadius: one_width * 30,
    paddingHorizontal: one_width * 20,
    paddingVertical: one_height >=0.6 ? one_height * 30 :one_height * 15
  },
  header_text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: one_height * 30,
  },
  footer_text: {
    color: '#05375a',
    fontSize: one_height * 22,
  },
  action: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingBottom: one_height >=0.6 ? one_height * 5:one_height * 2.5,
    borderBottomWidth: one_height * 1.5,
    borderBottomColor: '#f2f2f2',
  },
  text_footer_textInput: {
    top: 10,
    alignSelf: 'center',
    textAlignVertical: 'center',
    fontSize: one_height * 18,
    flex: 1,
    paddingLeft: one_width * 7,
    color: '#05375a',
  },
  bottom: {
    alignItems: 'center',
    marginTop:one_height >=0.6 ? one_height * 40:one_height*10
  },
  signIn: {
    width: width * 0.9,
    height: one_height * 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSignIn: {
    fontSize: one_height * 22,
    fontWeight: 'bold',
    color: 'white',
  },
});
