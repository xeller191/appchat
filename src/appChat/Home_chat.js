import React, {Component} from 'react';
import {Text, View, Button,StyleSheet} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';

export default class Home_chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        "chat" : {
          "-MCpbovg-Uqh2emL7fco" : {
            "member1" : {
              "-MCfZ_BArtcUrK5RLuYq" : {
                "email" : "test3@mail.com",
                "id" : "3",
                "lastname" : "test",
                "name" : "test3",
                "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
                "tel" : "09712345687",
                "timestamp" : "2020-07-20 16:43:31"
              }
            },
            "member2" : {
              "-MCfPbFwLGOYhWvnINgT" : {
                "email" : "test2@mail.com",
                "id" : "2",
                "lastname" : "test",
                "name" : "test2",
                "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
                "tel" : "0879956255",
                "timestamp" : "2020-07-20 16:44:59"
              }
            },
            "messages" : {
              "-MCpbp047Lr-2dU0mhCS" : {
                "message" : "แชท1",
                "message_from" : "-MCfZ_BArtcUrK5RLuYq",
                "time" : "15:23",
                "message_from_name":"art",

              },
              "-MCpcE5H0NNvyn658Xu0" : {
                "message" : "แชท3",
                "message_from" : "-MCfPbFwLGOYhWvnINgT",
                "time" : "15:24"
              },
              "-MCpbwB-2alGsaS6twAn" : {
                "message" : "แชท2",
                "message_from" : "-MCfZ_BArtcUrK5RLuYq",
                "time" : "15:24"
              },
              "-MCpcln1BPGna0bc7VDa" : {
                "message" : "ดวงดวาวส่องแสงลงมาตั้งไกล",
                "message_from" : "-MCfPbFwLGOYhWvnINgT",
                "time" : "15:27"
              },
              "-MCpd5JWAemR4-iGlV0Y" : {
                "message" : "แก้มน้องนางนั้นแดงกว่าใคร",
                "message_from" : "-MCfZ_BArtcUrK5RLuYq",
                "time" : "15:29"
              },
              "-MCpdPxGDrInRad2pMrO" : {
                "message" : "ใจพี่จมแทบพสุธา",
                "message_from" : "-MCfPbFwLGOYhWvnINgT",
                "time" : "15:29"
              }
            }
          },
          "-MCpdiZ37VKEiXpdQv2o" : {
            "member1" : {
              "-MCfgVdlPLlwdPCQ_jDq" : {
                "email" : "test4@mail.com",
                "id" : "4",
                "lastname" : "Tester",
                "name" : "Tester4",
                "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
                "tel" : "061223345",
                "timestamp" : "2020-07-20 17:08:16"
              }
            },
            "member2" : {
              "-MCfOX3tpFwP7t2lBbUq" : {
                "email" : "test1@mail.com",
                "id" : "1",
                "lastname" : "test",
                "name" : "test1",
                "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
                "tel" : "06412345678",
                "timestamp" : "2020-07-20 15:45:19"
              }
            },
            "messages" : {
              "-MCpdieA_bQN5Hsg8L98" : {
                "message" : "ฝากดวงใจพี่ลอยล่องไป",
                "message_from" : "-MCfgVdlPLlwdPCQ_jDq",
                "time" : "15:25"
              },
              "-MCpe41Z-MXN6DG_4C_q" : {
                "message" : "ไม่มีคำลาจากเธอสักคำ",
                "message_from" : "-MCfOX3tpFwP7t2lBbUq",
                "time" : "15:29"
              },
              "-MCpeBYo3wxPWBhAcYHQ" : {
                "message" : "ก็คงได้รู้แล้วว่าเธอไม่จริงใจ",
                "message_from" : "-MCfOX3tpFwP7t2lBbUq",
                "time" : "15:30"
              },
              "-MCpeNpViH9So35xIQNq" : {
                "message" : "เธอยังมีเยื่อใย ไปจากฉันง่ายดายโดยไม่มีร่ำลา",
                "message_from" : "-MCfgVdlPLlwdPCQ_jDq",
                "time" : "15:31"
              },
              "-MCpeYly5EdCH_khnOYm" : {
                "message" : "กับคนที่เธอเคยชิดใกล้",
                "message_from" : "-MCfOX3tpFwP7t2lBbUq",
                "time" : "15:32"
              },
              "-MCpefOCaPwILx60Sykr" : {
                "message" : "คนที่ทนเจ็บช้ำ คือ...",
                "message_from" : "-MCfOX3tpFwP7t2lBbUq",
                "time" : "15:33"
              },
              "-MCpep4qyOt9IvfmASAl" : {
                "message" : "เรา",
                "message_from" : "-MCfOX3tpFwP7t2lBbUq",
                "time" : "15:35"
              }
            }
          }
        },
        "group" : {
          "-MCpMJAhXjs7QnJNg691" : {
            "group_name" : "Big Ass",
            "member" : {
              "-MCpMJN3m130DqXwkrao" : [ "-MCfPbFwLGOYhWvnINgT", "-MCfZ_BArtcUrK5RLuYq", "-MCfgVdlPLlwdPCQ_jDq" ]
            },
            "messages" : {
              "-MCpMJGkjd5mv4kXAe0u" : {
                "message" : "พรหมลิขิต",
                "message_from" : "-MCfgVdlPLlwdPCQ_jDq",
                "time" : "14:11"
              }
            }
          },
          "-MCpVQddhR_yF-bQeabt" : {
            "group_name" : "POTATO",
            "member" : {
              "-MCfPbFwLGOYhWvnINgT" : {
                "email" : "test2@mail.com",
                "id" : "2",
                "lastname" : "test",
                "name" : "test2",
                "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
                "tel" : "0879956255",
                "timestamp" : "2020-07-20 16:44:59"
              },
              "-MCfZ_BArtcUrK5RLuYq" : {
                "email" : "test3@mail.com",
                "id" : "3",
                "lastname" : "test",
                "name" : "test3",
                "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
                "tel" : "09712345687",
                "timestamp" : "2020-07-20 16:43:31"
              },
              "-MCfgVdlPLlwdPCQ_jDq" : {
                "email" : "test4@mail.com",
                "id" : "4",
                "lastname" : "Tester",
                "name" : "Tester4",
                "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
                "tel" : "061223345",
                "timestamp" : "2020-07-20 17:08:16"
              }
            },
            "messages" : {
              "-MCpVQjiRFQ4Ibum1Fcy" : {
                "message" : "พรหมลิขิต",
                "message_from" : "-MCfOX3tpFwP7t2lBbUq",
                "time" : "14:51"
              }
            }
          }
        },
        "user" : {
          "-MCfOX3tpFwP7t2lBbUq" : {
            "email" : "test1@mail.com",
            "id" : "1",
            "lastname" : "test",
            "name" : "test1",
            "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
            "tel" : "06412345678",
            "timestamp" : "2020-07-20 15:45:19"
          },
          "-MCfPbFwLGOYhWvnINgT" : {
            "email" : "test2@mail.com",
            "id" : "2",
            "lastname" : "test",
            "name" : "test2",
            "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
            "tel" : "0879956255",
            "timestamp" : "2020-07-20 16:44:59"
          },
          "-MCfZ_BArtcUrK5RLuYq" : {
            "email" : "test3@mail.com",
            "id" : "3",
            "lastname" : "test",
            "name" : "test3",
            "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
            "tel" : "09712345687",
            "timestamp" : "2020-07-20 16:43:31"
          },
          "-MCfgVdlPLlwdPCQ_jDq" : {
            "email" : "test4@mail.com",
            "id" : "4",
            "lastname" : "Tester",
            "name" : "Tester4",
            "profile_img" : "https://upload.wikimedia.org/wikipedia/commons/7/7e/Circle-icons-profile.svg",
            "tel" : "061223345",
            "timestamp" : "2020-07-20 17:08:16"
          }
        }
      }
      ,
      data_news: [],
      uid : "-MCfZ_BArtcUrK5RLuYq"
    }
  }
  // get News A
  sentNews = () => {
    let feedItemKeys = [];
    let feedItems = [];
    let chat = [];
    let message_1 =[]
    let message_1_key =[]
    for (let i = 0; i < Object.keys(this.state.data.chat).length; i++) {
      // Object.keys(response.data.result) = ค่าkey ex.0039ee11-f1ac-4813-9af0-471448617b0a
      // console.log(this.state.data.chat[Object.keys(this.state.data.chat)[i]])
      message_1 = this.state.data.chat[Object.keys(this.state.data.chat)[i]].messages
      for (let j =0; j<Object.keys(message_1).length; j++){
        message_1_key.push({
          key_key:Object.keys(message_1)[j],
          data_message:message_1[Object.keys(message_1)[j]],
          // sender: '',
          // reciever: ''
        })
      }
      // console.log(Object.keys(message_1).length,message_1_key);
      // console.log(message_1_key[3].data_message);
      // console.log(this.state.data.chat[Object.keys(this.state.data.chat)[i]].receiver[Object.keys(this.state.data.chat[Object.keys(this.state.data.chat)[i]].receiver)]);
      feedItems.push({
        key: Object.keys(this.state.data.chat)[i],
        data: this.state.data.chat[Object.keys(this.state.data.chat)[i]],
        member1: this.state.data.chat[Object.keys(this.state.data.chat)[i]].member1[Object.keys(this.state.data.chat[Object.keys(this.state.data.chat)[i]].member1)] ,//this.state.data.chat[Object.keys(this.state.data.chat)[i]].receiver =>มัดเป็นก้อนเดียวหมายถึงการเข้าถึงrecieve
        member2: this.state.data.chat[Object.keys(this.state.data.chat)[i]].member2[Object.keys(this.state.data.chat[Object.keys(this.state.data.chat)[i]].member1)], //this.state.data.chat[Object.keys(this.state.data.chat)[i]].receiver =>มัดเป็นก้อนเดียวหมายถึงการเข้าถึงrecieve
        message_key:message_1_key[i],
        
      }); //response.data คือการเข้าถึงข้อมูล
      // feedItemKeys.push(Object.keys(response.data.result)[i])
      
    }
    // console.log(message_1_key);
    this.setState({data_news: message_1_key});
    
    
    
  };
  // get News B
  componentDidMount() {
    this.sentNews();
    setInterval(() => {
      this.sentNews();
      // console.log("::::::",this.state.data_news);
    }, 5000);
  }
  
  render() {
    return (
      <View style={styles.container}>
      <FlatList
        // showsVerticalScrollIndicator={false}
        indicatorStyle="white"
        data={this.state.data_news}
        renderItem={({item}) => 
        <View>
          {item.data_message.message_from === this.state.uid ?(
            <View style={{marginTop:15}}>
              <View style={{alignSelf:'flex-end',padding:2}}>
                <Text>{item.data_message.time}</Text>
              </View>
              <View style={{backgroundColor:'rgba(9,171,254,1)',alignSelf:'flex-end',padding:10,borderRadius:50}}>
                <Text style={styles.item,{color:'white',textAlign:"right",fontSize:16}}>{item.data_message.message}</Text>
              </View>
            </View>
            
          ):(
            <View style={{marginTop:15}}>
              <View style={{alignSelf:'flex-start',padding:2}}>
                <Text>{item.data_message.time}</Text>
              </View>
              <View style={{backgroundColor:'rgba(0,0,0,0.2)',alignSelf:'flex-start',padding:10,borderRadius:50}}>
                <Text style={styles.item,{color:'black',left:0,fontSize:16}}>{item.data_message.message}</Text>
              </View>
            </View>

          )}
          
        </View> }
      />
    </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    padding:30
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});