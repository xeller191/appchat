import React, { Component } from 'react'
import { Text, View ,StyleSheet,Dimensions,KeyboardAvoidingView} from 'react-native'
import { TextInput } from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';

const {width, height} = Dimensions.get('window');
const one_width = width/411.42857142857144
const one_height = height/797.7142857142857
export default class DataTelephone extends Component {
    constructor(props){
        super(props)
        this.state = {
            numberPhone:''
        }
    }
    render() {
        return (
            <KeyboardAvoidingView style={styles.container} >
                <Animatable.Text animation="bounceIn" duration={2000} style={styles.text_1}>โปรดระบุเบอร์โทรของคุณ</Animatable.Text>
                <View style={styles.container_textInput}>
                    <View style={styles.container_textInput_Name}>
                        <TextInput
                        style={styles.textInput1}
                            keyboardType="number-pad"
                            allowFontScaling={false}
                            label="เบอร์โทรศัพท์"
                            value={this.state.numberPhone}
                            onChangeText={(e)=>{this.setState({numberPhone:e})}}
                            theme={{
                                colors: {
                                        placeholder: 'white', text: 'white', primary: 'white',
                                        underlineColor: 'transparent', background: '#05375a'
                                }
                            }}
                            />
                    </View>

                </View>
                <View style={styles.button}>
                    <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('Home');
                    }}>
                    <LinearGradient
                        colors={['#5db8fe', '#39cff2']}
                        style={styles.signIn}>
                        <Text style={styles.textSignIn} allowFontScaling={false}>Next</Text>
                        <MaterialIcons name="navigate-next" size={one_height*35} color="white" />
                    </LinearGradient>
                    </TouchableOpacity>
                </View>

            </KeyboardAvoidingView>
        )
    }
}
const styles = StyleSheet.create({
    container :{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    text_1:{
        fontSize:one_height*25,
        marginBottom:one_height>=0.6? one_height*40:one_height*20
    },
    container_textInput:{
        width:one_width*300
    },
    container_textInput_Name:{
        margin:one_height >=0.6 ? one_height*5:one_height*2.5
    },
    button: {
        alignItems: 'flex-end',
        marginTop: one_height*220,

    },
    signIn: {
        width: one_width*150,
        height: one_height*40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: one_height*50,
        flexDirection: 'row',
    },
    textSignIn: {
        color: 'white',
        fontWeight: 'bold',
        fontSize:one_height*18,
        paddingLeft:one_width*15
    },
    textInput1:{
        fontSize:one_height*18
    }
})