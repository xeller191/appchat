import * as React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const {width, height} = Dimensions.get('window');
const one_width = width / 411.42857142857144;
const one_height = height / 797.7142857142857;

const getTabBarIcon = (props) => {
  const {route} = props;
  if (route.key === 'group') {
    return (
      <MaterialCommunityIcons name="account-group" size={25} color={'white'} />
    );
  } else if (route.key === 'notification') {
    return <FontAwesome name="home" size={25} color={'white'} />;
  } else {
    return <FontAwesome name="home" size={25} color={'white'} />;
  }
};
const FirstRoute = () => (
  <View style={[styles.scene, {backgroundColor: 'white'}]} />
);

const groupScreen = () => (
  <View style={[styles.scene, {backgroundColor: 'white'}]} />
);
const notificationScreen = () => {
  <View style={[styles.scene, {backgroundColor: 'white'}]} />;
};
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        {key: 'Home', title: 'Home'},
        {key: 'group', title: 'group'},
        {key: 'notification', title: 'notification'},
      ],
    };
  }

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({
          Home: FirstRoute,
          group: groupScreen,
          notification: notificationScreen,
        })}
        onIndexChange={(index) => this.setState({index})}
        initialLayout={{height: 100, width: Dimensions.get('window').width}}
        renderTabBar={(props) => (
          <TabBar
            {...props}
            indicatorStyle={{backgroundColor: '#e6f9ff', height: '7%'}}
            pressColor="yellow"
            renderIcon={(props) => getTabBarIcon(props)}
            // tabStyle={styles.bubble}
            labelStyle={styles.noLabel}
            activeColor="red"
            style={{color: 'red'}}
          />
        )}
        tabBarPosition={'top'}
        co
      />
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  noLabel: {
    display: 'none',
    height: 0,
  },
  bubble: {
    backgroundColor: 'lime',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 10,
  },
});
