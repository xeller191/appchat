import React,{ Component } from 'react';
import {Dimensions } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome' 
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicons from 'react-native-vector-icons/Ionicons' 
import Octicons from 'react-native-vector-icons/Octicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Home_home from './Home_home';
import Home_group from './Home_group';
import Home_video from './Home_chat';
import Home_shop from './Home_all_chat';
import Home_notification from './Home_notification';
import Home_setting from './Home_setting';
import Home_chat from './Home_chat';
import Home_all_chat from './Home_all_chat';

const Tab = createBottomTabNavigator();
// responsive ui A
const {width,height} = Dimensions.get('window')
const width_onePixel = width/411.42857142857144
const height_onePixel = height/845.7142857142857
// responsive ui A

export default class Home extends Component {
  constructor(props){
    super(props)
    this.state={
      role_member:""
    }
  }

  render() {
      return (
        <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, sizeID }) => {
            let iconName;
            if (route.name === 'Home_home') {
              iconName = focused //ถ้ามัน อยู่ที่นั้นหน้านั้นจะเป็น True
                ? "home": "home-outline"
              sizeID = focused ? height_onePixel*30 : height_onePixel*25
              return <Ionicons name={iconName} size={sizeID} color={color} />;
            } 
            else if (route.name === 'Home_group') {
              iconName = focused ? 'account-group' : 'account-group-outline'
              sizeID = focused ? height_onePixel*30 : height_onePixel*25
              return <MaterialCommunityIcons name={iconName} size={sizeID} color={color} />;
            } 
            else if (route.name === 'Home_chat') {
              iconName = focused ? 'chatbubble-ellipses' : 'chatbubble-ellipses-outline'
              sizeID = focused ? height_onePixel*30 : height_onePixel*25
              return <Ionicons name={iconName} size={sizeID} color={color} />;
            } else if (route.name === 'Home_all_chat') {
              iconName = focused ? 'ios-chatbubbles' : 'ios-chatbubbles-outline'
              sizeID = focused ? height_onePixel*30 : height_onePixel*25
              return <Ionicons name={iconName} size={sizeID} color={color} />;
            } 
            else if (route.name === 'Home_notification') {
              iconName = focused ? 'bell-ring' : 'bell-ring-outline'
              sizeID = focused ? height_onePixel*30 : height_onePixel*25
              return <MaterialCommunityIcons name={iconName} size={sizeID} color={color} />;
            } 
            else if (route.name === 'Home_setting') {
              iconName = focused ? 'three-bars' : 'three-bars'
              sizeID = focused ? height_onePixel*30 : height_onePixel*25
              return <Octicons name={iconName} size={sizeID} color={color} />;
            } 
            

           
          },
        })}

        tabBarOptions={{
          showLabel:false,
          activeTintColor: '#05375a',
          inactiveTintColor: 'gray',
          labelStyle: {
            fontSize: 15,
          },
          style: {
            backgroundColor: 'white',
            height:height_onePixel*50,
            padding:height_onePixel*3
          },
        }}
      >
        <Tab.Screen name="Home_home" component={Home_home}/>
        <Tab.Screen name="Home_group" component={Home_group} />
        <Tab.Screen name="Home_chat" component={Home_chat} />
        <Tab.Screen name="Home_all_chat" component={Home_all_chat} /> 
        <Tab.Screen name="Home_notification" component={Home_notification} /> 
        <Tab.Screen name="Home_setting" component={Home_setting} />

      </Tab.Navigator>
      )
  }
}
