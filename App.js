import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native'; //add Navigation (NavigationContainer)
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack'; //add Navigation (NavigationContainer)
import SplashComponent from './src/SplashComponent';
import SignInComponent from './src/SignInComponent';
import SingUpComponent from './src/SingUpComponent';
import Home from './src/appChat/Home';
import DataUser from './src/appChat/DataUser';
import DataTelephone from './src/appChat/DataTelephone';

const Stack = createStackNavigator();
//
const config = {
  animation: 'spring',
  config: {
    stiffness: 100,
    damping: 10,
    mass: 2,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};
//
//add createStackNavigator A **ส่วนเสริม
function MyStack() {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      headerMode={false}
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        transitionSpec: {
          open: config,
          close: config,
        },
      }}>
      <Stack.Screen name="SplashComponent" component={SplashComponent} />
      <Stack.Screen name="SignInComponent" component={SignInComponent} />
      <Stack.Screen name="SingUpComponent" component={SingUpComponent} />
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="DataUser" component={DataUser} />
      <Stack.Screen name="DataTelephone" component={DataTelephone} />
    </Stack.Navigator>
  );
}
//add createStackNavigator A **ส่วนเสริม

class App extends Component {
  render() {
    return (
      // add Navigation (NavigationContainer) A
      <NavigationContainer>
        <MyStack />
      </NavigationContainer>
      // add Navigation (NavigationContainer) B
    );
  }
}

export default App;
